
from gi.repository import Gtk

class MobinDialog():
	def __init__(self,text,title,both,window):

		self.res = None

		if both:
			self.dialog = Gtk.Dialog(title,window,0,
                     ("Cancel", Gtk.ResponseType.CANCEL,"Accept", Gtk.ResponseType.OK))
		else:
			self.dialog = Gtk.Dialog(title,window,0,
                     (Gtk.STOCK_OK, Gtk.ResponseType.OK))

		label = Gtk.Label(text)
		box = self.dialog.get_content_area()
		box.add(label)
		self.dialog.show_all()
		response = self.dialog.run()
		if response == Gtk.ResponseType.OK:
		 	self.res = True

		elif response == Gtk.ResponseType.CANCEL:
			self.res = False
		 	# dialog.destroy()

	def getResponse(self):
		return self.res

	def closeDialog(self):
		self.dialog.destroy()

class MobinMessageDialog():
	def __init__(self,first_text,secondary_text,message_type,window):
		self.res = None
		if message_type == Gtk.MessageType.INFO:
			self.dialog = Gtk.MessageDialog(window, 0, Gtk.MessageType.INFO,
	            ("Accept", Gtk.ResponseType.YES), first_text)
			self.dialog.format_secondary_text(secondary_text)
			self.dialog.run()
			# self.dialog.destroy()
		elif message_type == Gtk.MessageType.ERROR:
			self.dialog = Gtk.MessageDialog(window, 0, Gtk.MessageType.ERROR,
            ("Cancel", Gtk.ResponseType.NO),first_text)
			self.dialog.format_secondary_text(secondary_text)
			self.dialog.run()
			# self.dialog.destroy()
		elif message_type == Gtk.MessageType.WARNING:
			self.dialog = Gtk.MessageDialog(window, 0, Gtk.MessageType.WARNING,
            ("Cancel", Gtk.ResponseType.CANCEL,"OK", Gtk.ResponseType.OK), first_text)
			self.dialog.format_secondary_text(secondary_text)
			response = self.dialog.run()
			if response == Gtk.ResponseType.OK:
				self.res = True
			elif response == Gtk.ResponseType.CANCEL:
				self.res = False
			# self.dialog.destroy()
		elif message_type == Gtk.MessageType.QUESTION:
			self.dialog = Gtk.MessageDialog(window, 0, Gtk.MessageType.QUESTION,("No", Gtk.ResponseType.NO,"Yes", Gtk.ResponseType.YES), first_text)
			self.dialog.format_secondary_text(secondary_text)
			response = self.dialog.run()
			if response == Gtk.ResponseType.YES:
				# print("QUESTION self.dialog closed by clicking YES button")
				self.res = True
			elif response == Gtk.ResponseType.NO:
				self.res = False
				# print("QUESTION self.dialog closed by clicking NO button")
			# self.dialog.destroy()
        # print("INFO dialog closed")

        # dialog.destroy()

	def getResponse(self):
		return self.res

	def closeDialog(self):
		self.dialog.destroy()
