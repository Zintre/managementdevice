import pygtk
pygtk.require("2.0")
# import gtk
from gi.repository import Gtk

class LoginWindow():
	def __init__(self,mobin):
		self.is_login = False
		self.builder = Gtk.Builder()
		self.builder.add_from_file("../GladeFiles/Login.glade")
		self.builder.connect_signals(self)
		self.entry_username = self.builder.get_object("entry_of_username")
		self.entry_of_pass = self.builder.get_object("entry_of_pass")
		self.window = self.builder.get_object("window_login")


		self.window_adduser = self.builder.get_object("window_adduser")
		self.entry_user = self.builder.get_object("entry_username_user")
		self.entry_pass = self.builder.get_object("entry_pass")
		self.entry_repeat_pass = self.builder.get_object("entry_repeat_pass")
		self.label_info = self.builder.get_object("label_info")

		self.mobin = mobin

	def get_status_login(self):
		return self.is_login

	def hideWindow(self):
		self.window.hide()

	def showWindow(self):
		self.window.show_all()

	def showWindowAddUsers(self):
		self.label_info.set_text("Mobin is a software for the people who need store, handle and controlling a lot of machines. This software provides all that you need for perform all your task.")
		self.window_adduser.show_all()

	def on_window_destroy(self,button):
		self.mobin.destroy()

	def validate_is_not_empty_tableuser(self):
		self.window_adduser.set_title("Add a User")
		mb = self.mobin.getMobinDataCursor()
		rows = mb.getTableUsers()
		if (rows == []):
			return False
		else:
			return True
		# return True

	def validateInfoNewUser(self,button):
		if self.entry_user.get_text() == "" or self.entry_pass.get_text() == "" or self.entry_repeat_pass.get_text() == "":
			text1 = "Error: "
			text2 = "The entry widgets is not fill"
			result = self.mobin.show_message_dialog(text1,text2,Gtk.MessageType.ERROR,self.window_adduser)
		elif self.entry_pass.get_text() != self.entry_repeat_pass.get_text():
			text1 = "Error: "
			text2 = "Passwords must match"
			result = self.mobin.show_message_dialog(text1,text2,Gtk.MessageType.ERROR,self.window_adduser)
		else:
			data = [self.entry_user.get_text(),self.entry_pass.get_text()]
			mb = self.mobin.getMobinDataCursor()
			mb.addUser(data)
			if mb.get_status_sql():
				self.showWindow()
				self.window_adduser.hide()
				mb.set_status_sql(False)
			else:
				text1 = "Error: "
				text2 = "User input error to the database"
				result = self.mobin.show_message_dialog(text1,text2,Gtk.MessageType.ERROR,self.window_adduser)




	def on_button_login_clicked(self,button):
		mb = self.mobin.getMobinDataCursor()
		value = mb.validateLogin(self.entry_username.get_text(),self.entry_of_pass.get_text())
		if value == True:
			self.mobin.setInfoUserLogin(self.entry_username.get_text(),self.entry_of_pass.get_text())
			self.hideWindow()
			self.mobin.showWindow()
		else:
			text1 = "Error: "
			text2 = value
			result = self.mobin.show_message_dialog(text1,text2,Gtk.MessageType.ERROR,self.window_adduser)
			

		

