
from gi.repository import Gtk


class deleteClientWindow():
	def __init__(self,mobin):
		self.count_elements = 1
		self.list_ids = {}
		self.builder = Gtk.Builder()
		self.builder.add_from_file("../GladeFiles/deleteClient.glade")
		self.builder.connect_signals(self)
		self.window = self.builder.get_object("window_client")
		self.window.set_title("Delete Client")
		self.comboboxtext_clients = self.builder.get_object("comboboxtext_clients")
		self.mobin = mobin

		self.mobindata = self.mobin.getMobinDataCursor()

		rows = self.mobindata.getDataClient()
		for row in rows:
			self.comboboxtext_clients.append_text(row[1])
			self.list_ids[row[1]]=row[0]
			self.count_elements+=1

	def updateClient(self):
		i = 1
		while i<=self.count_elements:
			self.comboboxtext_clients.remove(1)
			i+=1
		self.list_ids.clear()
		self.count_elements = 1
		rows = self.mobindata.getDataClient()
		for row in rows:
			self.comboboxtext_clients.append_text(row[1])
			self.list_ids[row[1]]=row[0]
			self.count_elements+=1
			# self.list_ids.append(row[0])
	
	def showWindow(self):
		self.window.set_keep_above(True)
		self.window.show_all()

	def pressButtonCancel(self,button):
		self.window.hide()

	def validateInformation(self,button):
		# print self.list_ids
		text_active = self.comboboxtext_clients.get_active_text()
		# num = text_active.find(" ")
		# print self.list_ids[text_active]
		if text_active != "Name" and text_active != None:

			text1 = "Are you sure do you want remove this client?: "
			text2 = text_active
			result = self.mobin.show_message_dialog(text1,text2,Gtk.MessageType.QUESTION,self.window)

			if result:
				self.mobindata.deleteClient(str(self.list_ids[text_active]))
				if self.mobindata.get_status_sql():
					self.mobindata.set_status_sql(False)
					self.mobin.updateListClient()
					self.window.hide()
				else:
					text1 = "Error: "
					text2 =  str(self.mobindata.get_error())
					result = self.mobin.show_message_dialog(text1,text2,Gtk.MessageType.ERROR,self.window)



class addClientWindow():
	def __init__(self,mobin):
		self.builder = Gtk.Builder()
		self.builder.add_from_file("../GladeFiles/addclient.glade")
		self.builder.connect_signals(self)
		self.window_addclient = self.builder.get_object("window_client")
		self.window_addclient.set_title("Add Client")

		#Get the others widgets
		self.entry_name = self.builder.get_object("entry_name")
		self.entry_contact = self.builder.get_object("entry_contact")
		self.entry_email = self.builder.get_object("entry_email")
		self.mobin = mobin
		# self.mobindata = mobindatabase.MobinDatabase('../fabianDB')


	def showWindow(self):
		self.entry_name.set_text("")
		self.entry_contact.set_text("")
		self.entry_email.set_text("")
		self.window_addclient.show_all()
		self.window_addclient.set_keep_above(True)

	def pressButtonCancel(self,button):
		self.window_addclient.hide()

	def validateInformation(self,button):
		if self.entry_name.get_text() == "" or self.entry_contact.get_text()== "" or self.entry_email.get_text()== "":

			text1 = "Error: "
			text2 =  "The entry widgets is not fill"
			result = self.mobin.show_message_dialog(text1,text2,Gtk.MessageType.ERROR,self.window_addclient)

		else:
			try:
				exist = False		
				mb = self.mobin.getMobinDataCursor()
				rows = mb.getDataClient()
				for row in rows:
					if row[1] == self.entry_name.get_text():
						exist = True

				if exist:
					text1 = "Error: "
					text2 =  "This name of client exist"
					result = self.mobin.show_message_dialog(text1,text2,Gtk.MessageType.ERROR,self.window_addclient)
				else:
					data = [self.entry_name.get_text(),self.entry_contact.get_text(),self.entry_email.get_text()]
					mb.insert_client(data)

					text1 = "Information saved successfully"
					text2 =  ""
					result = self.mobin.show_message_dialog(text1,text2,Gtk.MessageType.INFO,self.window_addclient)

					mb.set_status_sql(False)
					self.mobin.updateListClient()
					self.window_addclient.hide()
			except:
				text1 = "Error: "
				text2 =  "Information do not saved"
				result = self.mobin.show_message_dialog(text1,text2,Gtk.MessageType.ERROR,self.window_addclient)


class ModifyClientWindow():
	def __init__(self,mobin):

		self.count_elements = 1
		self.list_ids = {}
		self.builder = Gtk.Builder()
		self.builder.add_from_file("../GladeFiles/modifyclient.glade")
		self.builder.connect_signals(self)
		self.window_modifyclient = self.builder.get_object("window_client")
		self.window_modifyclient.set_title("Modify Client")

		#Get the others widgets
		self.entry_name = self.builder.get_object("entry_name")
		self.entry_contact = self.builder.get_object("entry_contact")
		self.entry_email = self.builder.get_object("entry_email")

		self.comboboxtext_clients = self.builder.get_object("comboboxtext_clients")
		self.mobin = mobin
		self.mobindata = self.mobin.getMobinDataCursor()

		rows = self.mobindata.getDataClient()
		# print rows
		for row in rows:
			self.comboboxtext_clients.append_text(row[1])
			self.list_ids[row[1]]=row[0]
			self.count_elements+=1
		# self.mobindata = mobindatabase.MobinDatabase('../fabianDB')


	def showWindow(self):
		self.entry_name.set_text("")
		self.entry_contact.set_text("")
		self.entry_email.set_text("")
		self.changedClient(self.comboboxtext_clients)
		self.window_modifyclient.show_all()
		self.window_modifyclient.set_keep_above(True)

	def updateClient(self):
		i = 1
		while i<=self.count_elements:
			self.comboboxtext_clients.remove(1)
			i+=1
		self.list_ids.clear()
		self.count_elements = 1
		rows = self.mobindata.getDataClient()
		for row in rows:
			self.comboboxtext_clients.append_text(row[1])
			self.list_ids[row[1]]=row[0]
			self.count_elements+=1
		self.comboboxtext_clients.set_active(0)

	def changedClient(self,widget):
		text_active = widget.get_active_text()
		if text_active != "Name":
			if text_active != None:
				rows = self.mobin.getMobinDataCursor().getDataClientId(str(self.list_ids[text_active]))
				# print rows[0]
				self.entry_name.set_text(rows[0][1])
				self.entry_contact.set_text(rows[0][2])
				self.entry_email.set_text(rows[0][3])

	def pressButtonCancel(self,button):
		self.window_modifyclient.hide()

	def validateInformation(self,button):

		if self.entry_name.get_text() == "" or self.entry_contact.get_text()== "" or self.entry_email.get_text()== "":

			text1 = "Error: "
			text2 =  "The entry widgets is not fill"
			result = self.mobin.show_message_dialog(text1,text2,Gtk.MessageType.ERROR,self.window_modifyclient)
	

		else:
			try:
				exist = False		
				mb = self.mobin.getMobinDataCursor()
				rows = mb.getDataClient()
				for row in rows:
					if row[1] == self.entry_name.get_text():
						exist = True

				if exist:
					text1 = "Error: "
					text2 =  "This name of client exist"
					result = self.mobin.show_message_dialog(text1,text2,Gtk.MessageType.ERROR,self.window_modifyclient)
				else:
					text_active = self.comboboxtext_clients.get_active_text()
					data = [str(self.list_ids[text_active]),self.entry_name.get_text(),self.entry_contact.get_text(),self.entry_email.get_text()]
					mb.updateClient(data)

					text1 = "Information saved successfully"
					text2 =  ""
					result = self.mobin.show_message_dialog(text1,text2,Gtk.MessageType.INFO,self.window_modifyclient)

					self.window_modifyclient.hide()
					mb.set_status_sql(False)
					self.mobin.updateListClient()
					self.mobin.updateList()
		
			except:
				text1 = "Error: "
				text2 =  "Information do not saved"
				result = self.mobin.show_message_dialog(text1,text2,Gtk.MessageType.ERROR,self.window_modifyclient)
		