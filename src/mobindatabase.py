#!/usr/bin/env python


import os
import sqlite3
from struct import unpack

class MobinDatabase:
  def __init__ (self,dbsqlite):
    """
    @param dbsqlite The filename of sqlite database.
    """
    # SQLite format 3 (header string)
    self.__magic = ('\x53', '\x51', '\x4c', '\x69', '\x74', '\x65', '\x20', '\x66', '\x6f', '\x72', '\x6d', '\x61', '\x74', '\x20', '\x33', '\x00')
    self.__dbsqlite = dbsqlite
    self.__cur = None
    # self.__treedevice = Node("root", None, None)
    self.__valid_file = False
    self.__conn = None
    self.__status_sql = False
    self.__error_string = None
    
    self.__validate_file()
    if self.__valid_file:
      self.__connect()
      if (self.__conn):
        self.__cur = self.__conn.cursor()
          
        # if (self.__if_enable_foreign_key() == False):
        #   self.__enable_foreign_key()
        #   self.__load_data()

  def get_error(self):
    """
    Return the error occurred.
    """
    return self.__error_string

  def get_status_sql(self):
    """
    Return status of SQL executed.
    """
    return self.__status_sql

  def set_status_sql(self,status):
    self.__status_sql = status

  def is_connected(self):
    """
    Return status of connection.
    """
    return self.__conn

  def __validate_file(self):
    """
    Verify valid format of file
    """
    try:
      with open(self.__dbsqlite, 'rb') as handle:
        s = unpack('cccccccccccccccc', handle.read(16))
        if s == self.__magic:
          self.__valid_file = True
    except IOError as e:
      print "I/O error({0}): {1}".format(e.errno, e.strerror)
      self.__error_string = "I/O error({0}): {1}".format(e.errno, e.strerror)


  def file_is_valid(self):
    """
    Return if file is a valid sqlite3.
    """
    return self.__valid_file

  def __connect (self):
    """
    Connect to database.
    """    
    try:
      self.__conn = sqlite3.connect(self.__dbsqlite)
    except sqlite3.Error as e:
      print "An error occurred:", e.args[0]

  def __execute_sql_select (self,sql):
    """
    Execute a select SQL.
    @param sql to execute.
    """
    self.__cur.execute(sql)
    rows = self.__cur.fetchall()
    return rows

  def getTableUsers(self):
    sql = "select * from users"
    rows = self.__execute_sql_select(sql)
    return rows

  def addUser(self,data):
    c = self.__cur.execute('select MAX(id) from users')
    max_id = c.fetchone()[0]
    if max_id == None:
      new_id = 0
    else:
      new_id = max_id + 1

    sql = "insert into users(id,username,password) values("
    sql = sql + str(new_id)+",'"+data[0]+"','"+data[1]+"')"
    self.__execute_sql(sql)

  def deletehost(self,id_host):
    # c.execute("delete from host where id="+str(id_host))
    sql = "delete from host where id="+str(id_host)
    self.__execute_sql(sql)

  def getPassHost (self,id_host):
    sql = "select password"
    sql = sql + " from host where id ="+str(id_host)
    rows = self.__execute_sql_select(sql)
    return rows

  def getDataHost (self):
    sql = "select client.name,host.name,host.ip_address,host.username,host.id,host.owner"
    # sql = "select host.owner ,host.name,host.ip_address,host.username,host.id"
    sql = sql + " from host join client on client.id=host.id_client order by (client.name)"
    rows = self.__execute_sql_select(sql)
    return rows

  def getCompleteDataHost (self):
    sql = "select client.name,host.name,host.ip_address,host.username,host.owner,host.password"
    sql = sql + " from host join client on client.id=host.id_client order by (client.name)"
    rows = self.__execute_sql_select(sql)
    return rows

  def deleteClient(self,id_client):
    # c.execute("delete from host where id="+str(id_host))
    sql = "select * from host where id_client="+str(id_client)
    rows = self.__execute_sql_select(sql)
    if rows == []:
      sql = "delete from client where id="+str(id_client)
      self.__execute_sql(sql)
    else:
      self.__error_string = "This client has a host associated, You can not eliminated"


  def getDataClient (self):
    sql = "select id,name,contact,email from client"
    rows = self.__execute_sql_select(sql)
    return rows

  def getDataClientId (self,id_):
    sql = "select id,name,contact,email from client where id="+str(id_)
    rows = self.__execute_sql_select(sql)
    return rows

  def validateLogin(self,username,password):
    is_user = False
    is_login = False
    result = None
    # conn = sqlite3.connect('../fabianDB')
    # c = conn.cursor()
    # c.execute('SELECT * FROM users')
    sql = "SELECT * FROM users"
    rows = self.__execute_sql_select(sql)
    for row in rows:
      if username == row[1]:
        is_user = True
        if password == row[2]:
          is_login = True
          result = True
        else:
          result = "Incorrect password"

    if is_user == False:
      result = 'Incorrect username'
    return result

  def updateClient(self,data):
    """
    data[0]  ==>  id client
    data[1]  ==>  name client
    data[2]  ==>  contact client
    data[3]  ==>  email client
    """
    sql = "update client set name='"+data[1]+"',contact='"+data[2]+"',email='"+data[3]+"' where id='"+data[0]+"'"
    # sql = "update client set name="+str(data[1])+",contact="+str(data[2])+",email="+str(data[3])+" where id="+str(data[0])
    # sql = "into client(id,name,contact,email) values("
    # sql = sql + str(new_id)+",'"+data[0]+"','"+data[1]+"','"+data[2]+"')"
    self.__execute_sql(sql)

  def updateHost(self,data):
    """
    data[0]  ==>  id host
    data[1]  ==>  name host
    data[2]  ==>  ip host
    data[3]  ==>  username host
    data[4]  ==>  password host
    data[5]  ==>  id client host
    """
    sql = "update host set name='"+data[1]+"',ip_address='"+data[2]+"',username='"+data[3]+"',password='"+data[4]+"',id_client='"+data[5]+"' where id='"+data[0]+"'"
    self.__execute_sql(sql)

  def insert_host(self,data):

    # c = self.__execute_sql_select('select MAX(id) from host')
    c = self.__cur.execute('select MAX(id) from host')
    max_id = c.fetchone()[0]
    if max_id == None:
      new_id = 0
    else:
      new_id = max_id + 1

    sql = "insert into host(id,name,ip_address,username,password,id_client,owner) values("
    sql = sql + str(new_id)+",'"+data[0]+"','"+data[1]+"','"+data[2]+"','"+data[3]+"','"+data[4]+"','"+data[5]+"')"
    self.__execute_sql(sql)

  def insert_client(self,data):
    c = self.__cur.execute('select MAX(id) from client')
    max_id = c.fetchone()[0]
    if max_id == None:
      new_id = 0
    else:
      new_id = max_id + 1

    sql = "insert into client(id,name,contact,email) values("
    sql = sql + str(new_id)+",'"+data[0]+"','"+data[1]+"','"+data[2]+"')"
    self.__execute_sql(sql)

  def __execute_sql (self,sql):
    """
    Execute a SQL.
    """
    self.__status_sql = False
    
    try:
      self.__cur.execute(sql)
      self.__status_sql = True
      self.__conn.commit()
      # self.__status_sql = False
    except sqlite3.Error as e:
      self.__conn.rollback()
      print "An error occurred: ", e.args[0]
      self.__error_string = e.args[0]
