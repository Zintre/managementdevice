import pygtk
pygtk.require("2.0")
# import gtk
from gi.repository import Gtk, Vte
from gi.repository import GLib, GObject
import mobindialog
import mobindatabase
import mobinlogin
import mobinclient
import mobinhost
import mobinimport
import mobinconfigparser
import os
import datetime
import ConfigParser


class TabLabel(Gtk.Box):
    __gsignals__ = {
        "close-clicked": (GObject.SIGNAL_RUN_FIRST, GObject.TYPE_NONE, ()),
    }
    def __init__(self, label_text):
        Gtk.Box.__init__(self)
        self.set_orientation(Gtk.Orientation.HORIZONTAL)
        self.set_spacing(5) # spacing: [icon|5px|label|5px|close]  
        
        # icon
        icon = Gtk.Image.new_from_stock(Gtk.STOCK_NETWORK, Gtk.IconSize.MENU)
        self.pack_start(icon, False, False, 0)
        
        # label 
        label = Gtk.Label(label_text)
        self.pack_start(label, True, True, 0)
        
        # close button
        button = Gtk.Button()
        button.set_relief(Gtk.ReliefStyle.NONE)
        button.set_focus_on_click(False)
        button.add(Gtk.Image.new_from_stock(Gtk.STOCK_CLOSE, Gtk.IconSize.MENU))
        button.connect("clicked", self.button_clicked)
        data =  ".button {\n" \
                "-GtkButton-default-border : 0px;\n" \
                "-GtkButton-default-outside-border : 0px;\n" \
                "-GtkButton-inner-border: 0px;\n" \
                "-GtkWidget-focus-line-width : 0px;\n" \
                "-GtkWidget-focus-padding : 0px;\n" \
                "padding: 0px;\n" \
                "}"
        provider = Gtk.CssProvider()
        provider.load_from_data(data)
        # 600 = GTK_STYLE_PROVIDER_PRIORITY_APPLICATION
        button.get_style_context().add_provider(provider, 600) 
        self.pack_start(button, False, False, 0)
        
        self.show_all()
    
    def button_clicked(self, button, data=None):
        self.emit("close-clicked")

	def hideAll(self):
		self.hide()





class Mobin():
	def __init__(self):
		self.database_name = "../database"
		self.numberWindow = 1
		self.id_host = []
		self.username_login = None
		self.password_login = None
		self.config = ConfigParser.ConfigParser()

		self.builder = Gtk.Builder()
		self.builder.add_from_file("../GladeFiles/main.glade")
		self.builder.connect_signals(self)
		self.window = self.builder.get_object("main_window")
		self.window.set_title("Mobin")
		self.comboboxtext_filter = self.builder.get_object("comboboxtext_filter")
		self.notebookTerminal = self.builder.get_object("notebook1")
		self.scrolledTreeViewMain = self.builder.get_object("scrolledwindow1")
		self.checkButtonShowList = self.builder.get_object("checkbutton_show_list")
		self.label_command = self.builder.get_object("label_command")
		self.label_command.set_text("Command:  ")
		self.terminal = Vte.Terminal()
		self.terminal.fork_command_full(Vte.PtyFlags.DEFAULT,os.environ['HOME'],["/bin/sh"],[],GLib.SpawnFlags.DO_NOT_REAP_CHILD,None,None,)
		self.terminal.set_default_colors()
		tab_label = TabLabel("Term")
		tab_label.connect("close-clicked", self.on_close_clicked, self.notebookTerminal, self.terminal)
		self.notebookTerminal.append_page(self.terminal, tab_label)

		self.treeView = self.builder.get_object("treeview1")
		# self.store = Gtk.ListStore(str, str, int)
		self.store = Gtk.ListStore(str, str, str, str, str)
		# self.treeiter = self.store.append(["Pedro","E. Knuth", 25])
		self.treeView.set_model(self.store)
		self.renderer = Gtk.CellRendererText()
		self.column_client = Gtk.TreeViewColumn("Client", self.renderer, text=0)
		self.column_name = Gtk.TreeViewColumn("Name", self.renderer, text=1)
		self.column_ip_address = Gtk.TreeViewColumn("Ip address", self.renderer, text=2)
		self.column_username = Gtk.TreeViewColumn("User", self.renderer, text=3)
		self.column_owner = Gtk.TreeViewColumn("Owner", self.renderer, text=4)

		self.treeView.append_column(self.column_client)
		self.treeView.append_column(self.column_name)
		self.treeView.append_column(self.column_ip_address)
		self.treeView.append_column(self.column_username)
		self.treeView.append_column(self.column_owner)


		self.comboboxtext_protocol = self.builder.get_object("comboboxtext_protocol")

		self.mobindata = mobindatabase.MobinDatabase(self.database_name)

		self.login = mobinlogin.LoginWindow(self)
		if self.login.get_status_login() == False:
			if self.login.validate_is_not_empty_tableuser():
				self.login.showWindow()
			else:
				self.login.showWindowAddUsers()

		rows = self.mobindata.getDataHost()
		for row in rows:
			self.treeiter = self.store.append([row[0],row[1],row[2],row[3],row[5]])
			self.id_host.append(row[4])

		self.addclient = mobinclient.addClientWindow(self)
		self.addHost = mobinhost.addHostWindow(self)
		self.modify_host = mobinhost.ModifyHostWindow(self)
		self.deleteclient = mobinclient.deleteClientWindow(self)
		self.modify_client = mobinclient.ModifyClientWindow(self)
		self.importwindow = mobinimport.ImportWindow(self)
		self.commandwindow = mobinconfigparser.ConfigParserWindow(self)

		"""
		START  comboboxtext_commands
		"""		
		# for i in self.config.items('COMMANDS'):
		# 	print "Comand: "+i[0]+" || funcion: "+i[1]
		# self.list_commands = {"uptime":"Active time" ,"w / who":"Who is connected","top":"Process List","ps -AlF":"Process","vmstat 3":"System Activity","nmap -sP RED/Mascara":"Scan network","nmap":"Scan Host","free":"Memoria","iostat":"CPU y device activity","netstat -atun":"Connections"}
		self.comboboxtext_commands = self.builder.get_object("comboboxtext_commands")
		self.label_desc_command = self.builder.get_object("label_desc_command")
		self.config.read('../config.cfg')
		for values in self.config.items('COMMANDS'):
			# print i
			self.comboboxtext_commands.append_text(values[0])
		self.comboboxtext_commands.set_active(0)
		self.label_desc_command.set_text(str(self.config.get('COMMANDS',self.comboboxtext_commands.get_active_text())))
		# self.label_desc_command.set_text(self.list_commands[self.comboboxtext_commands.get_active_text()])

		"""
		END  comboboxtext_commands
		"""

		"""
		START  right button menu
		"""
		self.window_seepass = self.builder.get_object("window_pass_host")
		self.window_seepass.set_title("Insert your pass")
		self.entry_pass_user = self.builder.get_object("entry_pass_user")
		self.menu = Gtk.Menu()
		values = ["See pass","Modifiy host","Modifiy client"]
		for i in range(3):
			menu_items = Gtk.MenuItem(values[i])
			self.menu.append(menu_items)
			menu_items.connect("activate", self.menuitem_response, values[i])
			menu_items.show()
		"""
		END right button menu
		"""

	def click_about_us(self,button):
		text = "This software will be useful for anybody who need management a differents devices\n\nThis program was developed by Fabian Ramirez\nEmail:fadra.bar@gmail.com"
		self.show_message_dialog("About us",text,Gtk.MessageType.INFO,self.window)
		# def show_message_dialog (self,first_text,secondary_text,message_type,window):
		# if (window == None):
		# 	window = self.window
		# dialog = mobindialog.MobinMessageDialog(first_text,secondary_text,message_type,window)
		# result = dialog.getResponse()
		# dialog.closeDialog()
		# return result


	def refreshListCommand(self):
		self.config.read('../config.cfg')
		# print self.comboboxtext_commands.remove(30)
		# self.comboboxtext_commands.remove(0)
		model = self.comboboxtext_commands.get_model()
		iter_ = model.get_iter_first()
		while iter_:
			# print iter_
			iter_ = model.iter_next(iter_)
			self.comboboxtext_commands.remove(0)
		for values in self.config.items('COMMANDS'):
			self.comboboxtext_commands.append_text(values[0])
		self.comboboxtext_commands.set_active(0)
		self.label_desc_command.set_text(str(self.config.get('COMMANDS',self.comboboxtext_commands.get_active_text())))

	def removeCommand(self,button):
		# remove_option(section, option)
		self.comboboxtext_commands.get_active_text()
		text1 = "Do you want remove?: "
		text2 = "Name: "+self.comboboxtext_commands.get_active_text()+"\nDescription: "+self.config.get('COMMANDS',self.comboboxtext_commands.get_active_text())
		result = self.show_message_dialog(text1,text2,Gtk.MessageType.QUESTION,self.window)
		if result:
			self.config.remove_option('COMMANDS', self.comboboxtext_commands.get_active_text())
			with open('../config.cfg', 'wb') as configfile:
				self.config.write(configfile)
			self.refreshListCommand()


	def button_add_command_clicked(self,button):
		self.commandwindow.showWindow()

	def showTreeViewList(self,button):
		if self.checkButtonShowList.get_active():
			self.scrolledTreeViewMain.set_policy(Gtk.PolicyType.NEVER, Gtk.PolicyType.ALWAYS)
		else:
			self.scrolledTreeViewMain.set_policy(Gtk.PolicyType.ALWAYS, Gtk.PolicyType.ALWAYS)


	def getWordsForSearch(self,entry):
		# print "change!  "+entry.get_text()
		text_filter = self.comboboxtext_filter.get_active_text()
		try:
			self.store.clear()
			rows = self.mobindata.getDataHost()
			self.id_host[:] = []
			for row in rows:
				if text_filter == "All":
					if entry.get_text().lower() in row[0].lower() or entry.get_text().lower() in row[1].lower() or entry.get_text().lower() in row[2].lower() or entry.get_text().lower() in row[3].lower() or entry.get_text().lower() in row[5].lower():
						self.treeiter = self.store.append([row[0],row[1],row[2],row[3],row[5]])
						self.id_host.append(row[4])
				elif text_filter == "Client":
					if entry.get_text().lower() in row[0].lower():
						self.treeiter = self.store.append([row[0],row[1],row[2],row[3],row[5]])
						self.id_host.append(row[4])
				elif text_filter == "Name":
					if entry.get_text().lower() in row[1].lower():
						self.treeiter = self.store.append([row[0],row[1],row[2],row[3],row[5]])
						self.id_host.append(row[4])
				elif text_filter == "Ip address":
					if entry.get_text().lower() in row[2].lower():
						self.treeiter = self.store.append([row[0],row[1],row[2],row[3],row[5]])
						self.id_host.append(row[4])
				elif text_filter == "User":
					if entry.get_text().lower() in row[3].lower():
						self.treeiter = self.store.append([row[0],row[1],row[2],row[3],row[5]])
						self.id_host.append(row[4])
				elif text_filter == "Owner":
					if entry.get_text().lower() in row[5].lower():
						self.treeiter = self.store.append([row[0],row[1],row[2],row[3],row[5]])
						self.id_host.append(row[4])
			self.window.show_all()
		except:
			print "Caracter erroneo!"

	def on_close_clicked(self,tab_label, notebook, tab_widget):
	    """ Callback for the "close-clicked" emitted by custom TabLabel widget. """
	    if (notebook.get_n_pages() > 1):
	    	notebook.remove_page(notebook.page_num(tab_widget))

	def changeComboboxText_commands(self,widget):
		# self.label_desc_command.set_text(self.list_commands[self.comboboxtext_commands.get_active_text()])
		if self.comboboxtext_commands.get_active_text()!=None:
			self.label_desc_command.set_text(self.config.get('COMMANDS',self.comboboxtext_commands.get_active_text()))
		# try:
		# except:
		# 	pass

	def pressButtonLoadCommand(self,button):
		page_num = self.notebookTerminal.get_current_page()
		wid = self.notebookTerminal.get_nth_page(page_num)
		length = len(self.comboboxtext_commands.get_active_text())
		if wid != None:
			wid.feed_child(self.comboboxtext_commands.get_active_text(),length)


	def menuitem_response(self, widget, string):
		if string == "See pass":
			self.window_seepass.show_all()
			self.entry_pass_user.set_text("")
		elif string == "Modifiy host":
			# self.window_modify_host.show_all()
			tree_selection = self.treeView.get_selection()
			(model, pathlist) = tree_selection.get_selected_rows()
			# print tree_selection.count_selected_rows()
			if tree_selection.count_selected_rows()>0:
				for path in pathlist:
					tree_iter = model.get_iter(path)
					client_name = model.get_value(tree_iter,0)
					name = model.get_value(tree_iter,1)
					ip_address = model.get_value(tree_iter,2)
					username = model.get_value(tree_iter,3)
					owner = model.get_value(tree_iter,4)
					data = [str(self.id_host[int(str(path))]),name,ip_address,username,self.mobindata.getPassHost(self.id_host[int(str(path))])[0][0],owner,client_name]
					self.modify_host.showWindow(data)

			else:
				text1 = "Error:"
				text2 = "You must select a row"
				result = self.show_message_dialog(text1,text2,Gtk.MessageType.ERROR,self.window)

		elif string == "Modifiy client":
			# print "algo"
			self.modify_client.showWindow()
			# print "Client Modifiy"
			# self.terminal.reset(False,True)

	def pressButtonCancelModifyHost(self,button):
		self.window_modify_host.hide()
			

	def confirmPassUser(self,button):
		if self.password_login == self.entry_pass_user.get_text():
			tree_selection = self.treeView.get_selection()
			(model, pathlist) = tree_selection.get_selected_rows()
			id_host_pass = None
			for path in pathlist:
				id_host_pass = self.id_host[int(str(path))]
			if id_host_pass != None:
				self.window_seepass.hide()
				password = self.mobindata.getPassHost(id_host_pass)
				text1 = "Password Host:"
				text2 = password[0][0]
				result = self.show_message_dialog(text1,text2,Gtk.MessageType.INFO,self.window)
		else:
			result = self.show_message_dialog("Error:","This pass is incorrect",Gtk.MessageType.ERROR,self.window)
			

	def pressCancelButtonWindowConfirmPass(self,button):
		self.window_seepass.hide()


	"""
	START Seccion for filechooser window
	"""
	def on_button_import_clicked(self,button):
		# self.window_filechooser.show_all()
		self.importwindow.showAll()


	def on_button_export_clicked(self,button):
		now = datetime.datetime.now()
		dialog_export = Gtk.FileChooserDialog("Please choose a directory", self.window,Gtk.FileChooserAction.SAVE,("Cancel", Gtk.ResponseType.CANCEL,"Save", Gtk.ResponseType.OK))
		dialog_export.set_default_size(800, 400)
		dialog_export.set_current_name("dbexport_"+str(now.day)+""+str(now.month)+""+str(now.year)+"_"+str(now.hour)+":"+str(now.minute)+":"+str(now.second))
		response = dialog_export.run()
		if response == Gtk.ResponseType.OK:
			p = os.popen('cp '+self.database_name+' -T '+dialog_export.get_filename(),"r")
		# elif response == Gtk.ResponseType.CANCEL:
		# 	print("Cancel clicked")
		dialog_export.destroy()
	"""
	END Seccion for filechooser window
	"""

	def setDataBaseName(self,name):
		self.database_name = name

	def setInfoUserLogin(self,user,password):
		self.username_login = user
		self.password_login = password

	def getUserLogin(self):
		return self.username_login

	def show_message (self,msg,title,both,window):
		if (window == None):
			window = self.window

		dialog = mobindialog.MobinDialog(msg,title,both,window)
		result = dialog.getResponse()
		dialog.closeDialog()

		return result

	def show_message_dialog (self,first_text,secondary_text,message_type,window):
		if (window == None):
			window = self.window

		dialog = mobindialog.MobinMessageDialog(first_text,secondary_text,message_type,window)
		result = dialog.getResponse()
		dialog.closeDialog()
		return result

	def destroy(self):
		Gtk.main_quit()

	def getMobinDataCursor(self):
		return self.mobindata

	def showWindow(self):
		self.window.show_all()

	def on_window_destroy(self, object, data=None):
		Gtk.main_quit()

	def on_click_addClient(self,button):
		self.addclient.showWindow()

	def on_click_addHost(self,button):
		self.addHost.showWindow()

	def on_click_deleteClient(self,button):
		self.deleteclient.showWindow()

	def updateListClient(self):
		self.addHost.updateClient()
		self.modify_host.updateClient()
		self.deleteclient.updateClient()
		self.modify_client.updateClient()

	def deleteHost(self,button):
		tree_selection = self.treeView.get_selection()
		(model, pathlist) = tree_selection.get_selected_rows()
		# print tree_selection.count_selected_rows()
		if tree_selection.count_selected_rows()>0:
			for path in pathlist:
				tree_iter = model.get_iter(path)
				name = model.get_value(tree_iter,1)
				ip_address = model.get_value(tree_iter,2)
				username = model.get_value(tree_iter,3)

				text1 = "Do you want remove this Host?:"
				text2 = "Name: "+name+"\nIP: "+ip_address+"\nUser: "+username
				result = self.show_message_dialog(text1,text2,Gtk.MessageType.QUESTION,self.window)
				if result:
					id_host_delete = self.id_host[int(str(path))]
					self.mobindata.deletehost(id_host_delete)
					if self.mobindata.get_status_sql():
						self.mobindata.set_status_sql(False)
						self.updateList()
					else:
						text1 = "Error:"
						text2 = "Failed to delete the selected host"
						result = self.show_message_dialog(text1,text2,Gtk.MessageType.ERROR,self.window)
		else:
			text1 = "Error:"
			text2 = "You must select a row"
			result = self.show_message_dialog(text1,text2,Gtk.MessageType.ERROR,self.window)


	def updateList(self):
		self.store.clear()
		rows = self.mobindata.getDataHost()
		self.id_host[:] = []
		for row in rows:
			self.treeiter = self.store.append([row[0],row[1],row[2],row[3],row[5]])
			self.id_host.append(row[4])
		self.window.show_all()

	def connectHost(self,treeview, path, view_column):
		page_num = self.notebookTerminal.get_current_page()
		wid = self.notebookTerminal.get_nth_page(page_num)
		tree_selection = self.treeView.get_selection()
		(model, pathlist) = tree_selection.get_selected_rows()
		tree_iter = model.get_iter(path)
		name = model.get_value(tree_iter,1)
		ip_address = model.get_value(tree_iter,2)
		username = model.get_value(tree_iter,3)
		# print "Name: ",name,"\nIP: ",ip_address
		protocol = self.comboboxtext_protocol.get_active_text()
		length = len(protocol+" "+username+"@"+name)
		if wid != None:
			wid.feed_child(protocol+" "+username+"@"+name,length)

	def menuRightClickTreeView(self,treeview,event):
		if event.button == 3:
			self.menu.popup(None,None, None, None, event.button, event.time)
        	pass


	def newTerminal(self,button):
		self.numberWindow+=1
		T = Vte.Terminal()
		T.fork_command_full(Vte.PtyFlags.DEFAULT,os.environ['HOME'],["/bin/sh"],[],GLib.SpawnFlags.DO_NOT_REAP_CHILD,None,None,)
		# v = self.notebookTerminal.insert_page(T,None,-1)


		# tab_widget = Gtk.TextView()
		tab_label = TabLabel("Term")
		tab_label.connect("close-clicked", self.on_close_clicked, self.notebookTerminal, T)
		self.notebookTerminal.append_page(T, tab_label)

		self.window.show_all()

	def showWindow(self):
		self.window.show_all()

	def onButtonConnectHost(self,button):
		# self.notebookTerminal.append_page(T,None)
		page_num = self.notebookTerminal.get_current_page()
		wid = self.notebookTerminal.get_nth_page(page_num)
		tree_selection = self.treeView.get_selection()
		(model, pathlist) = tree_selection.get_selected_rows()
		for path in pathlist :
			tree_iter = model.get_iter(path)
			name = model.get_value(tree_iter,1)
			ip_address = model.get_value(tree_iter,2)
			username = model.get_value(tree_iter,3)
			# print "Name: ",name,"\nIP: ",ip_address
			protocol = self.comboboxtext_protocol.get_active_text()
			length = len(protocol+" "+username+"@"+name)
			if wid != None:
				wid.feed_child(protocol+" "+username+"@"+name,length)
				# wid.feed_child("ssh "+username+"@"+name,length)
		# print "element"

if __name__ == "__main__":
	mobin = Mobin()
	
	Gtk.main()	
