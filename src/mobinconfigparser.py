from gi.repository import Gtk
import ConfigParser

class ConfigParserWindow():
	def __init__(self,mobin):
		self.builder = Gtk.Builder()
		# self.builder.add_from_file("../GladeFiles/command.glade")
		self.builder.add_from_file("../GladeFiles/command.glade")
		self.builder.connect_signals(self)
		self.window_command = self.builder.get_object("window_add_command")
		self.window_command.set_title("Add Commnad")
		self.label_name_comand = self.builder.get_object("label_name_comand")
		self.label_name_comand.set_text("Command name: ")
		self.label_description = self.builder.get_object("label_description")
		self.label_description.set_text("Command descriprion: ")

		self.entry_name = self.builder.get_object("entry_name")
		self.entry_description = self.builder.get_object("entry_description")

		self.config = ConfigParser.ConfigParser()
		self.config.read('../config.cfg')
		self.mobin = mobin

		# set(section, option, value)

	def showWindow(self):
		self.window_command.show_all()
		self.entry_name.set_text("")
		self.entry_description.set_text("")
		self.window_command.set_keep_above(True)

	def pressCancel(self,button):
		self.window_command.hide()

	def pressAccept(self, button):
		if self.entry_description.get_text() == "" or self.entry_name.get_text()== "":
			text1 = "Error:"
			text2 = "The entry widgets is not fill"
			result = self.mobin.show_message_dialog(text1,text2,Gtk.MessageType.ERROR,self.window_command)
		else:
			exist = False
			for values in self.config.items('COMMANDS'):
				if values[0] == self.entry_name.get_text():
					exist = True
					break
			if exist==False:
				self.config.set("COMMANDS", self.entry_name.get_text(), self.entry_description.get_text())
				with open('../config.cfg', 'wb') as configfile:
					self.config.write(configfile)
				self.window_command.hide()
				self.mobin.refreshListCommand()
			else:
				text1 = "Error:"
				text2 = "The name of command exist"
				result = self.mobin.show_message_dialog(text1,text2,Gtk.MessageType.ERROR,self.window_command)
		# print "press"
