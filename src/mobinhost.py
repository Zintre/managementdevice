
from gi.repository import Gtk

class addHostWindow():
	def __init__(self,mobin):
		self.count_elements = 1
		self.builder = Gtk.Builder()
		# self.builder.add_from_file("GladeFiles/addclient.glade")
		self.builder.add_from_file("../GladeFiles/addHost.glade")
		self.builder.connect_signals(self)
		self.window_addhost = self.builder.get_object("window_host")
		self.window_addhost.set_title("Add Host")
		self.checkbutton = self.builder.get_object("checkbutton1")
		self.comboboxtext_clients = self.builder.get_object("comboboxtext_clients")

		#Get the others widgets
		self.entry_name = self.builder.get_object("entry_name")
		self.entry_ipaddress = self.builder.get_object("entry_ipaddress")
		self.entry_username = self.builder.get_object("entry_username")
		self.entry_password = self.builder.get_object("entry_password")
		self.mobin = mobin
		self.mobindata = self.mobin.getMobinDataCursor()
		self.client_with_id = {}
		rows = self.mobindata.getDataClient()
		for row in rows:
			# self.comboboxtext_clients.append_text(str(row[0])+" || "+row[1]+" || "+row[2])
			self.comboboxtext_clients.append_text(row[1])
			# self.comboboxtext_clients.append_text(row[1])
			self.client_with_id[row[1]]=row[0]
			self.count_elements+=1
		# print self.client_with_id

	def updateClient(self):
		i = 1
		self.client_with_id = {}
		while i<=self.count_elements:
			self.comboboxtext_clients.remove(1)
			i+=1

		self.count_elements = 1
		rows = self.mobindata.getDataClient()
		for row in rows:
			# self.comboboxtext_clients.append_text(str(row[0])+" || "+row[1]+" || "+row[2])
			self.comboboxtext_clients.append_text(row[1])
			self.client_with_id[row[1]]=row[0]
			self.count_elements+=1
		# conn.close()


	def showWindow(self):
		self.entry_name.set_text("")
		self.entry_ipaddress.set_text("")
		self.entry_username.set_text("")
		self.entry_password.set_text("")

		self.window_addhost.show_all()
		self.window_addhost.set_keep_above(True)

	def pressButtonCancel(self,button):
		self.window_addhost.hide()
		# print self.comboboxtext_clients.get_active_text()[0]


	def showPassword(self,check):
		if self.checkbutton.get_active():
			self.entry_password.set_visibility(True)
		else:
			self.entry_password.set_visibility(False)


	def validateInformation(self,button):
		text_active = self.comboboxtext_clients.get_active_text()
		# num = text_active.find(" ")

		if self.entry_name.get_text() == "" or self.entry_ipaddress.get_text()== "" or self.entry_username.get_text()== "" or self.entry_password.get_text()=="":
			text1 = "Error:"
			text2 = "The entry widgets is not fill"
			result = self.mobin.show_message_dialog(text1,text2,Gtk.MessageType.ERROR,self.window_addhost)

		elif text_active== "Name":
			text1 = "Error:"
			text2 = "You must select a client"
			result = self.mobin.show_message_dialog(text1,text2,Gtk.MessageType.ERROR,self.window_addhost)
		else:
			try:
				owner = self.mobin.getUserLogin()				
				data = [self.entry_name.get_text(),self.entry_ipaddress.get_text(),self.entry_username.get_text(),self.entry_password.get_text(),str(self.client_with_id[text_active]),owner]
				self.mobindata.insert_host(data)

				text1 = "Information saved successfully"
				text2 = ""
				result = self.mobin.show_message_dialog(text1,text2,Gtk.MessageType.INFO,self.window_addhost)


				self.mobindata.set_status_sql(False)
				self.mobin.updateList()
				self.window_addhost.hide()
			except:
				text1 = "Error:"
				text2 = "Information do not saved"
				result = self.mobin.show_message_dialog(text1,text2,Gtk.MessageType.ERROR,self.window_addhost)


class ModifyHostWindow():
	def __init__(self,mobin):
		self.data_modify = []
		self.count_elements = 1
		self.builder = Gtk.Builder()
		# self.builder.add_from_file("GladeFiles/addclient.glade")
		self.builder.add_from_file("../GladeFiles/addHost.glade")
		self.builder.connect_signals(self)
		self.window_addhost = self.builder.get_object("window_host")
		self.window_addhost.set_title("Modify Host")

		self.checkbutton = self.builder.get_object("checkbutton1")
		self.comboboxtext_clients = self.builder.get_object("comboboxtext_clients")

		#Get the others widgets
		self.entry_name = self.builder.get_object("entry_name")
		self.entry_ipaddress = self.builder.get_object("entry_ipaddress")
		self.entry_username = self.builder.get_object("entry_username")
		self.entry_password = self.builder.get_object("entry_password")
		self.mobin = mobin
		self.mobindata = self.mobin.getMobinDataCursor()
		self.client_with_id = {}
		rows = self.mobindata.getDataClient()
		for row in rows:			
			self.comboboxtext_clients.append_text(row[1])
			self.client_with_id[row[1]]=row[0]
			self.count_elements+=1

	def updateClient(self):
		i = 1
		self.client_with_id = {}
		while i<=self.count_elements:
			self.comboboxtext_clients.remove(1)
			i+=1

		self.count_elements = 1
		rows = self.mobindata.getDataClient()
		for row in rows:
			# self.comboboxtext_clients.append_text(str(row[0])+" || "+row[1]+" || "+row[2])
			self.comboboxtext_clients.append_text(row[1])
			self.client_with_id[row[1]]=row[0]
			self.count_elements+=1
		# conn.close()


	def showWindow(self,data_modify):
		self.data_modify = data_modify
		self.entry_password.set_visibility(False)
		self.checkbutton.set_active(False)
		self.entry_name.set_text(self.data_modify[1])
		self.entry_ipaddress.set_text(self.data_modify[2])
		self.entry_username.set_text(self.data_modify[3])
		self.entry_password.set_text(self.data_modify[4])
		# iter_ = self.comboboxtext_clients.get_active_iter()
		model = self.comboboxtext_clients.get_model()
		iter_ = model.get_iter_first()
		while iter_:
			iter_ = model.iter_next(iter_)
			if model.get_value(iter_, 0) == self.data_modify[6]:
				self.comboboxtext_clients.set_active_iter(iter_)
				break

		self.window_addhost.show_all()
		self.window_addhost.set_keep_above(True)

	def pressButtonCancel(self,button):
		self.window_addhost.hide()
		# print self.comboboxtext_clients.get_active_text()[0]


	def showPassword(self,check):
		if self.checkbutton.get_active():
			self.entry_password.set_visibility(True)
		else:
			self.entry_password.set_visibility(False)


	def validateInformation(self,button):
		text_active = self.comboboxtext_clients.get_active_text()
		# num = text_active.find(" ")

		if self.entry_name.get_text() == "" or self.entry_ipaddress.get_text()== "" or self.entry_username.get_text()== "" or self.entry_password.get_text()=="":
			text1 = "Error:"
			text2 = "The entry widgets is not fill"
			result = self.mobin.show_message_dialog(text1,text2,Gtk.MessageType.ERROR,self.window_addhost)

		elif text_active== "Name":
			text1 = "Error:"
			text2 = "You must select a client"
			result = self.mobin.show_message_dialog(text1,text2,Gtk.MessageType.ERROR,self.window_addhost)
		else:
			try:
				if self.mobin.getUserLogin() == self.data_modify[5]:
					owner = self.mobin.getUserLogin()
				else:
					owner = self.mobin.getUserLogin() +"|"+ self.data_modify[5]

				data = [self.data_modify[0],self.entry_name.get_text(),self.entry_ipaddress.get_text(),self.entry_username.get_text(),self.entry_password.get_text(),str(self.client_with_id[text_active]),owner]

				self.mobindata.updateHost(data)

				text1 = "Information update successfully"
				text2 = ""
				result = self.mobin.show_message_dialog(text1,text2,Gtk.MessageType.INFO,self.window_addhost)


				self.mobindata.set_status_sql(False)
				self.mobin.updateList()
				self.window_addhost.hide()
			except:
				text1 = "Error:"
				text2 = "Information do not saved"
				result = self.mobin.show_message_dialog(text1,text2,Gtk.MessageType.ERROR,self.window_addhost)