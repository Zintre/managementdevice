
from gi.repository import Gtk
import mobindatabase

class ImportWindow():
	def __init__(self,mobin):
		self.mobin = mobin
		self.aux_namedatabase = None
		self.builder = Gtk.Builder()
		self.builder.add_from_file("../GladeFiles/import_databasewindow.glade")
		self.builder.connect_signals(self)
		# self.window_filechooser = self.builder.get_object("window_filechooser")
		self.window_filechooser = self.builder.get_object("window_import")
		self.window_filechooser.set_title("Select a database file")
		self.filechooser = self.builder.get_object("filechooserbutton")
		self.combobox_kindelement = self.builder.get_object("comboboxtext_kindelement")

		self.treeView_left = self.builder.get_object("treeview_left")
		self.treeView_right = self.builder.get_object("treeview_right")

		self.store_left_host = Gtk.ListStore(str, str, str, str, str, str)
		self.store_right_host = Gtk.ListStore(str, str, str, str, str, str)
		self.store_left_client = Gtk.ListStore(str, str, str)
		self.store_right_client = Gtk.ListStore(str, str, str)

		self.mobindb = self.mobin.getMobinDataCursor()

		mode = self.combobox_kindelement.get_active_text()
		if mode == "Host":
			self.loadHostViews()
		elif mode == "Client":
			self.loadClientViews()
		# self.store = Gtk.ListStore(str, str, int)
		

	def loadHostViews(self):
		columns_right = self.treeView_right.get_columns()
		columns_left = self.treeView_left.get_columns()
		if columns_right != [] or columns_left != []:
			for column in columns_right:
				self.treeView_right.remove_column(column)
			for column in columns_left:
				self.treeView_left.remove_column(column)

		
		self.treeView_right.set_model(self.store_right_host)
		self.treeView_left.set_model(self.store_left_host)
		self.renderer = Gtk.CellRendererText()

		self.column_client_right = Gtk.TreeViewColumn("Client", self.renderer, text=0)
		self.column_name_right = Gtk.TreeViewColumn("Name", self.renderer, text=1)
		self.column_ip_address_right = Gtk.TreeViewColumn("Ip address", self.renderer, text=2)
		self.column_username_right = Gtk.TreeViewColumn("User", self.renderer, text=3)
		self.column_owner_right = Gtk.TreeViewColumn("Owner", self.renderer, text=4)
		self.column_pass_right = Gtk.TreeViewColumn("Pass", self.renderer, text=5)

		self.column_client_left = Gtk.TreeViewColumn("Client", self.renderer, text=0)
		self.column_name_left = Gtk.TreeViewColumn("Name", self.renderer, text=1)
		self.column_ip_address_left = Gtk.TreeViewColumn("Ip address", self.renderer, text=2)
		self.column_username_left = Gtk.TreeViewColumn("User", self.renderer, text=3)
		self.column_owner_left = Gtk.TreeViewColumn("Owner", self.renderer, text=4)
		self.column_pass_left = Gtk.TreeViewColumn("Pass", self.renderer, text=5)

		self.treeView_left.append_column(self.column_client_left)
		self.treeView_left.append_column(self.column_name_left)
		self.treeView_left.append_column(self.column_ip_address_left)
		self.treeView_left.append_column(self.column_username_left)
		self.treeView_left.append_column(self.column_owner_left)
		self.treeView_left.append_column(self.column_pass_left)

		self.treeView_right.append_column(self.column_client_right)
		self.treeView_right.append_column(self.column_name_right)
		self.treeView_right.append_column(self.column_ip_address_right)
		self.treeView_right.append_column(self.column_username_right)
		self.treeView_right.append_column(self.column_owner_right)
		self.treeView_right.append_column(self.column_pass_right)

	def loadClientViews(self):
		columns_right = self.treeView_right.get_columns()
		columns_left = self.treeView_left.get_columns()
		if columns_right != [] or columns_left != []:
			for column in columns_right:
				self.treeView_right.remove_column(column)
			for column in columns_left:
				self.treeView_left.remove_column(column)

		self.treeView_right.set_model(self.store_right_client)
		self.treeView_left.set_model(self.store_left_client)
		self.renderer = Gtk.CellRendererText()

		self.column_name_right = Gtk.TreeViewColumn("Name", self.renderer, text=0)
		self.column_contact_right = Gtk.TreeViewColumn("Contact", self.renderer, text=1)
		self.column_email_right = Gtk.TreeViewColumn("Email", self.renderer, text=2)

		self.column_name_left = Gtk.TreeViewColumn("Name", self.renderer, text=0)
		self.column_contact_left = Gtk.TreeViewColumn("Contact", self.renderer, text=1)
		self.column_email_left = Gtk.TreeViewColumn("Email", self.renderer, text=2)

		self.treeView_left.append_column(self.column_name_left)
		self.treeView_left.append_column(self.column_contact_left)
		self.treeView_left.append_column(self.column_email_left)

		self.treeView_right.append_column(self.column_name_right)
		self.treeView_right.append_column(self.column_contact_right)
		self.treeView_right.append_column(self.column_email_right)


	def pressLoadButton(self,button):
		try:
			database_name = self.filechooser.get_filename()
			if database_name == None:
				text1 = "Error:"
				text2 = "You must select a file"
				result = self.mobin.show_message_dialog(text1,text2,Gtk.MessageType.ERROR,self.window_filechooser)
			else:
				if self.aux_namedatabase == database_name:
					# self.mobin.show_message("You already have loaded this database","Warning",False,self.window_filechooser)
					text1 = "You already have loaded this database"
					text2 = ""
					result = self.mobin.show_message_dialog(text1,text2,Gtk.MessageType.INFO,self.window_filechooser)
				else:
					self.aux_namedatabase = database_name
					mode = self.combobox_kindelement.get_active_text()
					if mode == "Host":
						self.store_left_host.clear()
					elif mode == "Client":
						self.store_left_client.clear()
					importdata = mobindatabase.MobinDatabase(database_name)
					if importdata.file_is_valid():
						if mode == "Host":
							rows = importdata.getCompleteDataHost()
							for row in rows:
								treeiter = self.store_left_host.append([row[0],row[1],row[2],row[3],row[4],row[5]])
						elif mode == "Client":
							rows = importdata.getDataClient()
							for row in rows:
								treeiter = self.store_left_client.append([row[1],row[2],row[3]])
							# print "Client"
					else:
						text1 = "Error:"
						text2 = "This file is not valid"
						result = self.mobin.show_message_dialog(text1,text2,Gtk.MessageType.ERROR,self.window_filechooser)
						# self.mobin.show_message("This file is not valid","Error",False,self.window_filechooser)
		except:
			text1 = "Error:"
			text2 = "A big error ocurred"
			result = self.mobin.show_message_dialog(text1,text2,Gtk.MessageType.ERROR,self.window_filechooser)
			# self.mobin.show_message("Error","Error",False,self.window_filechooser)

	def addHostToLeftTreeView(self,button):
		mode = self.combobox_kindelement.get_active_text()
		tree_selection = self.treeView_left.get_selection()
		(model, pathlist) = tree_selection.get_selected_rows()
		if mode == "Host":
			for path in pathlist :
				tree_iter = model.get_iter(path)
				client = model.get_value(tree_iter,0)
				name = model.get_value(tree_iter,1)
				ip_address = model.get_value(tree_iter,2)
				username = model.get_value(tree_iter,3)
				owner = model.get_value(tree_iter,4)
				password = model.get_value(tree_iter,5)
				treeiter = self.store_right_host.append([client,name,ip_address,username,owner,password])
				self.store_left_host.remove(tree_iter)
		elif mode == "Client":
			for path in pathlist :
				tree_iter = model.get_iter(path)
				name = model.get_value(tree_iter,0)
				contact = model.get_value(tree_iter,1)
				email = model.get_value(tree_iter,2)
				treeiter = self.store_right_client.append([name,contact,email])
				self.store_left_client.remove(tree_iter)


	def addHostToRightTreeView(self,button):
		mode = self.combobox_kindelement.get_active_text()
		tree_selection = self.treeView_right.get_selection()
		(model, pathlist) = tree_selection.get_selected_rows()
		if mode == "Host":
			for path in pathlist :
				tree_iter = model.get_iter(path)
				client = model.get_value(tree_iter,0)
				name = model.get_value(tree_iter,1)
				ip_address = model.get_value(tree_iter,2)
				username = model.get_value(tree_iter,3)
				owner = model.get_value(tree_iter,4)
				password = model.get_value(tree_iter,5)
				treeiter = self.store_left_host.append([client,name,ip_address,username,owner,password])
				self.store_right_host.remove(tree_iter)
		elif mode == "Client":
			for path in pathlist :
				tree_iter = model.get_iter(path)
				name = model.get_value(tree_iter,0)
				contact = model.get_value(tree_iter,1)
				email = model.get_value(tree_iter,2)
				treeiter = self.store_left_client.append([name,contact,email])
				self.store_right_client.remove(tree_iter)
			



	def pressButtonCancel_filechooser(self,button):
		self.window_filechooser.hide()

	def pressButtonAccept_filechooser(self,button):
		mode = self.combobox_kindelement.get_active_text()
		if mode == "Host":
			rows = self.mobindb.getDataClient()
			model = self.treeView_right.get_model()
			aux_data = []
			datas_to_add = []

			exist = False
			tree_iter = model.get_iter_first()
			while tree_iter != None:
				exist = False
				# print row[1] +" | "+ model.get_value(tree_iter,0)
				for row in rows:
					if row[1] == model.get_value(tree_iter,0):
						exist = True
						aux_data = [model.get_value(tree_iter,1),model.get_value(tree_iter,2),model.get_value(tree_iter,3),model.get_value(tree_iter,5),str(row[0]),model.get_value(tree_iter,4)]
						break
				datas_to_add.append(aux_data)
				if exist==False:
					# print "This element: "+model.get_value(tree_iter,0)+" exist"
					# self.mobin.show_message("This client "+model.get_value(tree_iter,0)+" do not exist, You must import this client","Warning",False,self.window_filechooser)
					text1 = "Error:"
					text2 = "This client "+model.get_value(tree_iter,0)+" do not exist, You must import this client"
					result = self.mobin.show_message_dialog(text1,text2,Gtk.MessageType.ERROR,self.window_filechooser)
					break
				tree_iter = model.iter_next(tree_iter)
			if exist!=False:
				"""
				Adding Host to database
				"""
				for data in datas_to_add:
					self.mobindb.insert_host(data)

				self.mobindb.set_status_sql(False)
				self.mobin.updateList()
				self.window_filechooser.hide()
		if mode == "Client":
			exist = False
			aux_data = []
			datas_to_add = []
			rows = self.mobindb.getDataClient()
			model = self.treeView_right.get_model()
			tree_iter = model.get_iter_first()
			while tree_iter != None:
				exist = False
				for row in rows:
					if row[1] == model.get_value(tree_iter,0):
						exist = True
						break
				if exist:
					# self.mobin.show_message("This client "+model.get_value(tree_iter,0)+" exist in database, You must change the name","Warning",False,self.window_filechooser)
					text1 = "Error:"
					text2 = "This client "+model.get_value(tree_iter,0)+" exist in database, You must change the name"
					result = self.mobin.show_message_dialog(text1,text2,Gtk.MessageType.ERROR,self.window_filechooser)
					break
				else:
					aux_data = [model.get_value(tree_iter,0),model.get_value(tree_iter,1),model.get_value(tree_iter,2)] #id,name,contact,email)
					datas_to_add.append(aux_data)
				tree_iter = model.iter_next(tree_iter)
			if exist==False:
				"""
				Adding Client to database
				"""
				for data in datas_to_add:
					self.mobindb.insert_client(data)
				self.mobindb.set_status_sql(False)
				self.mobin.updateListClient()
				self.window_filechooser.hide()

		# print self.filechooser.get_filename()


	def changedAD(self,widget):
		mode = self.combobox_kindelement.get_active_text()
		self.aux_namedatabase = None
		self.store_left_host.clear()
		self.store_left_client.clear()
		self.store_right_client.clear()
		self.store_right_host.clear()
		if mode == "Host":
			self.loadHostViews()
		elif mode == "Client":
			self.loadClientViews()

	def showAll(self):
		self.window_filechooser.show_all()
		self.store_left_host.clear()
		self.store_left_client.clear()
		self.store_right_client.clear()
		self.store_right_host.clear()
		self.aux_namedatabase = None
		self.window_filechooser.set_keep_above(True)
