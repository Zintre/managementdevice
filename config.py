import ConfigParser

config = ConfigParser.RawConfigParser()

# When adding sections or items, add them in the reverse order of
# how you want them to be displayed in the actual file.
# In addition, please note that using RawConfigParser's and the raw
# mode of ConfigParser's respective set functions, you can assign
# non-string values to keys internally, but will receive an error
# when attempting to write to a file or when you get it in non-raw
# mode. SafeConfigParser does not allow such assignments to take place.

#self.list_commands = {"uptime":"Active time" ,"w / who":"Who is connected",
#"top":"Process List","ps -AlF":"Process","vmstat 3":"System Activity",
#"nmap -sP RED/Mascara":"Scan network","nmap":"Scan Host","free":"Memoria",
#"iostat":"CPU y device activity","netstat -atun":"Connections"}
config.add_section('COMMANDS')
config.set('COMMANDS', 'free', 'Memory')
config.set('COMMANDS', 'uptime', 'Active time')
config.set('COMMANDS', 'w / who', 'Who is connected')
config.set('COMMANDS', 'top', 'Process List')
config.set('COMMANDS', 'ps -AlF', 'Process')
config.set('COMMANDS', 'vmstat 3', 'System Activity')
config.set('COMMANDS', 'nmap -sP RED/Mascara', 'Scan network')
config.set('COMMANDS', 'nmap', 'Scan Host')
config.set('COMMANDS', 'iostat', 'CPU and device activity')
config.set('COMMANDS', 'netstat -atun', 'Connections')

# Writing our configuration file to 'example.cfg'
with open('config.cfg', 'wb') as configfile:
     config.write(configfile)
